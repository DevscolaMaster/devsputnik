# Devsputnik

Devsputnik is meant to be a template for Devscola's cloud server, currently hosted at [Hetzner](https://hetzner.com) and now holding [Devscloud](https://cloud.devscola.org), a custom [Nextcloud](https://nextcloud.com/) instance where we keep our school's files, books and project boards, but it's meant to be upgraded and expanded with new tools in the near future. Feel free to contribute!

This repo is based on a fork of a fork of [Gramola](https://gitlab.com/devscola/gramola) and its purpose is to open up the infrastructure to the whole community and make possible the immediate recreation of the Devsputnik server as-is, should circumstances require it, or why not, be forked, contributed, edited or adapted however you wish for whatever purposes you might deem useful! ;)

## Recreating the server

1. Install [Terraform](https://terraform.io) (v.0.14 as of today)
2. Log into Devscola's [Hetzner](https://cloud.hetzner.com) account
3. Go to project Devsputnik, generate and API token and copy it
4. Clone this project
5. Create a `terraform.tfvars` file in the root of the project
6. Set the API token as an `hcloud_token` environment variable (ex: `hcloud_token="API_TOKEN"`)
7. Run `terraform init` to install all the required plugins
8. Run `terraform apply` and confirm to recreate the server in Hetzer.

## Destroying the server
- Run `terraform destroy` and confirm to destroy the server but...

BE CAREFUL! This means data loss forever, so you better NOT DO IT unless you know what you are doing and really want to do it! Don't worry too much about doing this by mistake on Devsputnik since we have the server locked by default and several backup snapshots, but still, try not to mess with this command on Devscola's server!

