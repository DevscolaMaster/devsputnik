
# Provider designation hetzner
terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "1.24.1"
    }
  }
}

provider "hcloud" {       
  token = var.hcloud_token                                          
}

# The token to connect to the hetzner API is specified in the terraform.tfvars file

# CREATING THE SERVER
resource "hcloud_server" "devsputnik" {      
  # Server name                           
  name = var.name 
  # OS Image                                           
  image = var.image      
  # Instance type (pricing plan)                                         
  server_type = var.server_type
  # Region                                   
  location = var.location
  # Enable backups                                        
  backups = "true"
  # SSH key(s)                                                 
  ssh_keys = [hcloud_ssh_key.devsputnik.id]
  # Configuration template (cloud-init)                       
  user_data = data.template_file.init.rendered  
  
  provisioner "file" {                                       
    source = "config/.zshrc"
    destination = "/root/.zshrc"
  }


  # provisioner "file" {                                       
  #   source = "config/nextcloud"
  #   destination = "/home/${var.username}/${var.subdomain}"
  # }

  connection {
    type = "ssh"
    host = self.ipv4_address
    timeout = "1m"
    private_key = local.private_key
  }
}

data template_file "init" {
  template = file("${path.module}/config/init.yml")

  vars = {
    server_name = var.name
    username = var.username
    public_key = local.public_key
    subdomain = var.subdomain
  }
}

# Definition of SSH key from variable
resource "hcloud_ssh_key" "devsputnik" {
  name = var.name
  public_key = local.public_key
}

locals {                        
  public_key = file("~/.ssh/devsputnik.pub")
  private_key = file("~/.ssh/devsputnik")
}